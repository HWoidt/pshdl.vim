pshdl.vim
=========


This is a vim plugin for pshdl_ .


Features
--------

Supported:
 - Syntax highlighting of pshdl files

Planned:
 - Localhelper/Commandline integration as make command

Installation
------------

Using pathogen_, installing is as easy as:

.. sourcecode:: sh

        $ cd ~/.vim/bundle
        $ git clone https://bitbucket.org/HWoidt/pshdl.vim.git

Alternatively, do something along the lines of:

.. sourcecode:: sh

        $ mkdir /tmp/vimpshdl
        $ cd /tmp/vimpshdl
        $ git clone https://bitbucket.org/HWoidt/pshdl.vim.git
        $ cp -r pshdl.vim/* ~/.vim/

.. _pshdl: http://pshdl.org 
.. _pathogen: https://github.com/tpope/vim-pathogen
