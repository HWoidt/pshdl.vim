" Vim ftdetect file
" Language:     pshdl
" Maintainer:   Hendrik Woidt
" Last Change:  4 Apr 2014
"

if did_filetype()	" filetype already set..
	  finish		" ..don't do these checks
endif

autocmd BufRead,BufNewFile *.pshdl set filetype=pshdl
