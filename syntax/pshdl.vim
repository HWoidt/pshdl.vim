" Vim Syntax file
" Language: PSHDL
" Maintainer: Hendrik Woidt
" Latest Revision: 2 April 2014

if exists("b:current_syntax")
        finish
endif

syn keyword pshdlKeywords       module interface package import
syn keyword pshdlKeywords       for if include generate 
syn keyword pshdlPrimitives     int uint bit
syn keyword pshdlDirections     const param in out inout
syn keyword pshdlReg            register
syn match   pshdlOperators      "[#><=&|~?:()]"

syn match   pshdlRange          "{[A-Za-z0-9_:. \-]\+}"

syn region  pshdlComment start="\/\*" end="\*\/" fold
syn match   pshdlComment "//.*$" contains=pshdlTodo
syn keyword pshdlTodo TODO FIXME XXX NOTE

" variable
syn match pshdlVars   '\$[A-Za-z_][A-Za-z0-9_.]*'

" annotation
"syn match pshdlAnnotation '@[A-Za-z]\+'
syn match pshdlAnnotation '@\w\+'

" string constant
syn match pshdlString '".*"'

" Integer with - + or nothing in front
syn match pshdlNumber '[A-Za-z_]\@<!\d\+'
syn match pshdlNumber '[A-Za-z_]\@<![-+]\d\+'

" Hex number
syn match pshdlNumber '[A-Za-z_]\@<!0x[[:digit:]a-fA-F]\+'

" Floating point number with decimal no E or e (+,-)
syn match pshdlNumber '[A-Za-z_]\@<!\d\+\.\d*'
syn match pshdlNumber '[A-Za-z_]\@<![-+]\d\+\.\d*'

" Floating point like number with E and no decimal point (+,-)
syn match pshdlNumber '[A-Za-z_]\@<![-+]\=\d[[:digit:]]*[eE][\-+]\=\d\+'
syn match pshdlNumber '[A-Za-z_]\@<!\d[[:digit:]]*[eE][\-+]\=\d\+'

" Floating point like number with E and decimal point (+,-)
syn match pshdlNumber '[A-Za-z_]\@<![-+]\=\d[[:digit:]]*\.\d*[eE][\-+]\=\d\+'
syn match pshdlNumber '[A-Za-z_]\@<!\d[[:digit:]]*\.\d*[eE][\-+]\=\d\+'


let b:current_syntax = "pshdl"
hi def link pshdlKeywords       Statement
hi def link pshdlPrimitives     Type
hi def link pshdlDirections     Statement
hi def link pshdlOperators      PreProc
hi def link pshdlVars           PreProc
hi def link pshdlAnnotation     PreProc
hi def link pshdlReg            Special
hi def link pshdlNumber         Constant
hi def link pshdlString         Constant
hi def link pshdlComment        Comment
hi def link pshdlTodo           Todo
hi def link pshdlRange          Special

